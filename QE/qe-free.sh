# General dependencies
apk add perl g++ linux-headers libarchive libarchive-tools

VERSION=$1

TOPDIR=`pwd`
export PATH=$PATH:$TOPDIR/openmpi/bin
mkdir -p $TOPDIR/bin/
BINDIR=$TOPDIR/bin/
LIBDIR=$TOPDIR/libs/

# QE - Free
cd $TOPDIR
wget https://gitlab.com/QEF/q-e/-/archive/qe-${VERSION}/q-e-qe-${VERSION}.tar.bz2
tar xjf q-e-qe-${VERSION}.tar.bz2
cd q-e-qe-${VERSION}



# Prepare libXC
mkdir -p libxc/lib
cp $LIBDIR/xc_f03_lib_m.mod libxc/
cp $LIBDIR/xc_version.h libxc/

cp $LIBDIR/libxc.a libxc/lib
cp $LIBDIR/libxcf03.a libxc/lib

./configure CFLAGS=" -no-pie -static -static-libgcc" FFLAGS="-O3 -g -no-pie -fallow-argument-mismatch -static -static-libgfortran" --with-libxc --with-libxc-prefix=`pwd`/libxc --with-libxc-include=`pwd`/libxc

# Prepare
cp $LIBDIR/libopenblas.a .
cp $LIBDIR/libfftw3.a .
cp $LIBDIR/fftw3.f03 ./FFTXlib/


sed 's/__FFTW/__FFTW3/' -i make.inc
sed 's/^LDFLAGS.*/LDFLAGS = -g -no-pie -static -static-libgfortran/g' -i make.inc
sed 's/^FFT_LIBS.*/FFT_LIBS=\$\(TOPDIR\)\/libfftw3.a/g' -i make.inc
sed 's/^BLAS_LIBS.*/BLAS_LIBS=\$\(TOPDIR\)\/libopenblas.a/g' -i make.inc
sed 's/^LAPACK_LIBS_SWITCH.*/LAPACK_LIBS_SWITCH = external/g' -i make.inc
sed 's/^LAPACK_LIBS .*/LAPACK_LIBS =/g' -i make.inc
sed 's/--disable-parallel/--disable-parallel --build=x86_64/g' -i install/extlibs_makefile
make pw ph pp neb cp

cp -L bin/* $BINDIR/
cp -L $TOPDIR/openmpi/bin/mpirun $BINDIR/

cd $BINDIR
ls

# remove all but a few selected executables
find . -type f -not -name pw.x -and \
               -not -name pp.x -and \
               -not -name projwfc.x \
               -not -name cp.x \
               -not -name hp.x \
               -not -name ph.x \
               -exec rm {} \;

for file in ./*
do
  gzip -9 $file
done

cd $TOPDIR
