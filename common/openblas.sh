# OpenBLAS
cd $TOPDIR
wget https://github.com/xianyi/OpenBLAS/releases/download/v0.3.15/OpenBLAS-0.3.15.zip
unzip OpenBLAS-0.3.15.zip
cd OpenBLAS-0.3.15
make BINARY=64 DYNAMIC_ARCH=1 > /dev/null
cp $TOPDIR/OpenBLAS-0.3.15/libopenblas.a $LIBDIR/
cd $TOPDIR
