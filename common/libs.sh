
export TOPDIR=`pwd`
export LIBDIR=$TOPDIR/libs/
export OMPIDIR=$TOPDIR/openmpi/

if [ -d "$LIBDIR" ]; then
  case "$CI_COMMIT_MESSAGE" in
  *openblas*)
    # just continue and rebuild stuff
    apk add perl g++ linux-headers libarchive libarchive-tools wget
    . ./common/openblas.sh
    ;;
  *fftw*)
    # just continue and rebuild stuff
    apk add perl g++ linux-headers libarchive libarchive-tools wget
    . ./common/fftw.sh
    ;;
  *mkl*)
    # just continue and rebuild stuff
    apk add perl g++ linux-headers libarchive libarchive-tools wget
    . ./common/mkl.sh
    ;;
  *libxc*)
    # just continue and rebuild stuff
    apk add perl g++ linux-headers libarchive libarchive-tools wget
    . ./common/libxc.sh
    ;;
  *openmpi*)
    # just continue and rebuild stuff
    apk add perl g++ linux-headers libarchive libarchive-tools wget
    . ./common/openmpi.sh
    ;;
  *alllibs*)
    # just continue and rebuild stuff
    apk add perl g++ linux-headers libarchive libarchive-tools wget
    . ./common/openblas.sh.
    . ./common/fftw.sh
    . ./common/mkl.sh
    . ./common/libxc.sh
    . ./common/openmpi.sh
    ;;
  *)
    exit
    ;;
  esac
else
  # Generate all dependencies
  mkdir -p $LIBDIR
  apk add perl g++ linux-headers libarchive libarchive-tools wget
  . ./common/openblas.sh.
  . ./common/fftw.sh
  . ./common/mkl.sh
  . ./common/libxc.sh
  . ./common/openmpi.sh
fi





