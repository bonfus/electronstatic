

# FFTW
cd $TOPDIR
wget http://fftw.org/fftw-3.3.9.tar.gz
tar xzf fftw-3.3.9.tar.gz
cd fftw-3.3.9/
./configure
make > /dev/null
cp $TOPDIR/fftw-3.3.9/.libs/libfftw3.a $LIBDIR/
cp $TOPDIR/fftw-3.3.9/api/fftw3.f03 $LIBDIR/
cd $TOPDIR
