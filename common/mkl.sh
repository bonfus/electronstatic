# MKL Intel
cd $TOPDIR
wget http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/16917/l_mkl_2020.4.304.tgz
#tar xvzf l_mkl_2020.4.304.tgz
tar xzf l_mkl_2020.4.304.tgz l_mkl_2020.4.304/rpm/intel-mkl-gnu-f-2020.4-304-2020.4-304.x86_64.rpm \
                             l_mkl_2020.4.304/rpm/intel-mkl-core-2020.4-304-2020.4-304.x86_64.rpm \
                             l_mkl_2020.4.304/rpm/intel-mkl-common-c-2020.4-304-2020.4-304.noarch.rpm \
                             l_mkl_2020.4.304/rpm/intel-mkl-common-f-2020.4-304-2020.4-304.noarch.rpm

cd l_mkl_2020.4.304/rpm/

bsdtar -xvf intel-mkl-gnu-f-2020.4-304-2020.4-304.x86_64.rpm
bsdtar -xvf intel-mkl-core-2020.4-304-2020.4-304.x86_64.rpm
bsdtar -xvf intel-mkl-common-c-2020.4-304-2020.4-304.noarch.rpm
bsdtar -xvf intel-mkl-common-f-2020.4-304-2020.4-304.noarch.rpm

cp ./opt/intel/compilers_and_libraries_2020.4.304/linux/mkl/lib/intel64_lin/libmkl_sequential.a $LIBDIR/
cp ./opt/intel/compilers_and_libraries_2020.4.304/linux/mkl/lib/intel64_lin/libmkl_core.a  $LIBDIR/
cp ./opt/intel/compilers_and_libraries_2020.4.304/linux/mkl/lib/intel64_lin/libmkl_gf_lp64.a   $LIBDIR/
cp ./opt/intel/compilers_and_libraries_2020.4.304/linux/mkl/include/mkl_dfti.f90 $LIBDIR/

#clean
cd $TOPDIR
rm -r l_mkl_2020.4.304*
cd $TOPDIR
