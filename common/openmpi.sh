# OpenMPI
cd $TOPDIR
wget https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-3.1.6.tar.bz2
tar xjf openmpi-3.1.6.tar.bz2
INSTALL_PATH="$OMPIDIR"
cd openmpi-3.1.6/
CFLAGS="-static --static" CXXFLAGS="-static --static" CC="gcc -no-pie -static -static-libgcc -static-libstdc++" CXX="g++ -no-pie -static -static-libgcc -static-libstdc++" F90="gfortran -no-pie -static -static-libgcc -static-libstdc++ -static-libgfortran" ./configure --without-memory-manager --without-libnuma --enable-static --disable-shared --enable-mca-static=pml --disable-mpi-profile --prefix=$INSTALL_PATH LDFLAGS="-static -static-libgcc -static-libstdc++" THREAD_LDFLAGS=-Wl,--no-export-dynamic  
make > /dev/null
make install
cd $TOPDIR
