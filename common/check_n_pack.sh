# MD5sums and torrents
apk add mktorrent

BINDIR=`pwd`/bin

cd $BINDIR &&
for file in *; do
    if [[ -f "$file" ]]; then
        md5sum -- "$file" > "${file}.md5"
        mktorrent -a "http://tracker.openbittorrent.com:80/announce" \
        -c "$file ($CI_JOB_ID, $CI_COMMIT_SHORT_SHA)" \
        -w "https://gitlab.com/bonfus/electronstatic/-/jobs/$CI_JOB_ID/artifacts/raw/bin/$file" \
        $file
    fi
done

tar czf all_torrents.tar.gz *.md5 *.torrent
