

# LibXC
cd $TOPDIR
wget --trust-server-names "https://www.tddft.org/programs/libxc/down.php?file=5.1.5/libxc-5.1.5.tar.gz"
tar xzf libxc-5.1.5.tar.gz
cd libxc-5.1.5
./configure --enable-static=yes
make > /dev/null
cp ./src/.libs/libxc.a $LIBDIR/
cp ./src/.libs/libxcf03.a $LIBDIR/
cp ./src/xc_f03_lib_m.mod $LIBDIR/
cp xc_version.h $LIBDIR/
cd $TOPDIR
